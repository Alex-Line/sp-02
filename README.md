https://gitlab.com/Alex-Line/sp-02.git

# Task Manager SP-02

## Developer
Aleksandr Linev

E-mail: uranus_123@mail.ru

## Software

* Maven 3.6.1
* Java 1.8
* MYSQL 8.0.18
* Spring 5.2.3.RELEASE
* PostgreSQL 42.2.12
* JRE
* IDE IntelliJ IDEA CE

## Build commands

    mvn clean
    mvn install

## Run command

    java -jar target/taskmanagersp-2.0-RELEASE.jar

