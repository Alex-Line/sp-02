package com.iteco.linealex.sp.endpoint;

import com.iteco.linealex.sp.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.sp.api.service.IProjectService;
import com.iteco.linealex.sp.dto.ProjectDto;
import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.user.LowAccessLevelException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;

@Component
@WebService(endpointInterface = "com.iteco.linealex.sp.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Autowired
    IProjectService projectService;

    @Nullable
    @Override
    @WebMethod
    public ProjectDto getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        @Nullable final Project project = projectService.getEntityById(entityId);
        if (project == null) return null;
        if (project.getUser().getId().equals(session.getUserId()) || session.getRole().equals(Role.ADMINISTRATOR))
            return Project.toProjectDto(project);
        return null;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntities()));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntities(userId)));
    }

    @Override
    @WebMethod
    public void persistProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projects", partName = "projects") @NotNull final Collection<ProjectDto> collection
    ) throws Exception {
        validateSession(session);
        for (@NotNull final ProjectDto project : collection) {
            projectService.persist(ProjectDto.toProject(project));
        }
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        @Nullable final Project project = projectService.getEntityById(entityId);
        if (project == null) throw new Exception("There is not such project!");
        if (!project.getUser().getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        projectService.removeEntity(entityId);
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR && !session.getUserId().equals(userId))
            throw new LowAccessLevelException();
        projectService.removeAllEntities(userId);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        projectService.removeAllEntities();
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable final ProjectDto entity
    ) throws Exception {
        validateSession(session);
        projectService.persist(ProjectDto.toProject(entity));
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable final ProjectDto entity
    ) throws Exception {
        validateSession(session);
        if (entity == null) throw new Exception("Null project!");
        @NotNull final Project project = ProjectDto.toProject(entity);
        if (!session.getUserId().equals(project.getUser().getId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        projectService.merge(project);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDto getProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        if (entityName == null) throw new Exception("Null project name!");
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        @Nullable final Project project = projectService.getEntityByName(entityName);
        if (project == null) throw new Exception("There is no such project!");
        return Project.toProjectDto(project);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDto getProjectByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        if (entityName == null) throw new Exception("Null project name!");
        if (userId == null) throw new Exception(("Null user id"));
        @Nullable final Project project = projectService.getEntityByName(userId, entityName);
        if (project == null) throw new Exception("There is no such project!");
        if (!project.getUser().getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return Project.toProjectDto(project);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        if (pattern == null) throw new Exception("Null pattern for search!");
        if (userId == null) throw new Exception(("Null user id"));
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesByName(userId, pattern)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        if (pattern == null) throw new Exception("Null pattern for search!");
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesByName(pattern)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesSortedByStartDate()));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        if (userId == null) throw new Exception("Null user id");
        if (!userId.equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesSortedByStartDate(userId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesSortedByFinishDate()));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        if (userId == null) throw new Exception("Null user id");
        if (!userId.equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesSortedByFinishDate(userId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesSortedByStatus()));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<ProjectDto> getAllProjectsSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        if (userId == null) throw new Exception("Null user id");
        if (!userId.equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntitiesSortedByStatus(userId)));
    }

}