package com.iteco.linealex.sp.context;

import com.iteco.linealex.sp.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.sp.api.endpoint.ISessionEndpoint;
import com.iteco.linealex.sp.api.endpoint.ITaskEndpoint;
import com.iteco.linealex.sp.api.endpoint.IUserEndpoint;
import com.iteco.linealex.sp.api.repository.IUserRepository;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @Value("${ADMIN}")
    private String adminLogin;

    @Value("${ADMIN_PASSWORD}")
    private String adminPassword;

    @Value("${USER}")
    private String userLogin;

    @Value("${USER_PASSWORD}")
    private String userPassword;

    @Value("${PASSWORD_SALT}")
    private String passwordSalt;

    @Value("${PASSWORD_TIMES}")
    private String passwordTimes;

    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    public void start() {
        init();
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
    }

    private void init() {
        try {
            userRepository.deleteAll();
            @NotNull final User admin = new User();
            admin.setLogin(adminLogin);
            admin.setHashPassword(TransformatorToHashMD5.getHash(
                    adminPassword, passwordSalt, Integer.parseInt(passwordTimes)));
            admin.setRole(Role.ADMINISTRATOR);
            userRepository.save(admin);
            @NotNull final User user = new User();
            user.setLogin(userLogin);
            user.setHashPassword(TransformatorToHashMD5.getHash(
                    userPassword, passwordSalt, Integer.parseInt(passwordTimes)));
            userRepository.save(user);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}