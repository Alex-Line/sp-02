package com.iteco.linealex.sp.exception.session;

import com.iteco.linealex.sp.exception.TaskManagerException;
import org.jetbrains.annotations.NotNull;

public class InvalidSessionException extends TaskManagerException {

    @Override
    public @NotNull String getMessage() {
        return "THE SESSION OF CURRENT USER IS INVALID";
    }

}