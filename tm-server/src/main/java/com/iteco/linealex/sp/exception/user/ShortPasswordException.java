package com.iteco.linealex.sp.exception.user;

import com.iteco.linealex.sp.exception.TaskManagerException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ShortPasswordException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "ENTERED PASSWORD IS TO SHORT. COMMAND WAS INTERRUPTED\n";
    }

}