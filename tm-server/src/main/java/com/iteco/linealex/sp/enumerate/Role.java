package com.iteco.linealex.sp.enumerate;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMINISTRATOR("administrator"),
    ORDINARY_USER("ordinary user");

    @NotNull
    private String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}