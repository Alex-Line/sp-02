package com.iteco.linealex.sp.exception.entity;

import com.iteco.linealex.sp.exception.TaskManagerException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class InsertExistingEntityException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "THERE IS SUCH ELEMENT ALREADY. COMMAND WAS INTERRUPTED\n";
    }

}