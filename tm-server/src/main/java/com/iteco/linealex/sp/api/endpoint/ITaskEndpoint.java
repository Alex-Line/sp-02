package com.iteco.linealex.sp.api.endpoint;

import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.dto.TaskDto;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    TaskDto getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void persistTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "tasks", partName = "tasks") @NotNull final Collection<TaskDto> collection
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception;

    @WebMethod
    void removeAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable final TaskDto entity
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable final TaskDto entity
    ) throws Exception;

    @Nullable
    @WebMethod
    TaskDto getTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception;

    @Nullable
    @WebMethod
    TaskDto getTaskByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @WebMethod
    void removeAllTasksFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @Nullable
    @WebMethod
    TaskDto getTaskByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByStartDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByFinishDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDto> getAllTasksSortedByStatusWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception;

}