package com.iteco.linealex.sp.api.endpoint;

import com.iteco.linealex.sp.dto.ProjectDto;
import com.iteco.linealex.sp.dto.SessionDto;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebFault;
import java.util.Collection;

@WebFault
@WebService
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface IProjectEndpoint {

    @WebMethod
    @Nullable
    ProjectDto getProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void persistProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projects", partName = "projects") @NotNull final Collection<ProjectDto> collection
    ) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String entityId
    ) throws Exception;

    @WebMethod
    void removeAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userID", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void persistProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable final ProjectDto entity
    ) throws Exception;

    @WebMethod
    void mergeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "project", partName = "project") @Nullable final ProjectDto entity
    ) throws Exception;

    @Nullable
    @WebMethod
    ProjectDto getProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception;

    @Nullable
    @WebMethod
    ProjectDto getProjectByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String entityName
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDto> getAllProjectsWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

}