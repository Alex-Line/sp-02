package com.iteco.linealex.sp.entity;

import com.iteco.linealex.sp.dto.TaskDto;
import com.iteco.linealex.sp.util.ApplicationUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tasks")
@NoArgsConstructor
public final class Task extends AbstractTMEntity implements Serializable {

    @Nullable
    @ManyToOne
    @Basic(optional = false)
    private Project project = null;

    @NotNull
    public static TaskDto toTaskDto(
            @NotNull final Task task
    ) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setUserId(task.getUser().getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setStatus(task.getStatus());
        taskDto.setDateStart(task.getDateStart());
        taskDto.setDateFinish(task.getDateFinish());
        if (task.getProject() == null) return taskDto;
        taskDto.setProjectId(task.getProject().getId());
        return taskDto;
    }

    @NotNull
    public static List<TaskDto> toTasksDto(
            @NotNull final List<Task> tasks
    ) {
        @NotNull final List<TaskDto> result = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            result.add(toTaskDto(task));
        }
        return result;
    }

    @NotNull
    @Override
    public String toString() {
        return "Task " + name + " {" +
                "ID = " + super.getId() +
                ", \n    description='" + description + '\'' +
                ", \n    Start date = " + ApplicationUtils.formatDateToString(dateStart) +
                ", \n    Finish date = " + ApplicationUtils.formatDateToString(dateFinish) +
                ", \n    Status = " + status.getName() +
                ", \n    PROJECT_ID = " + project.getId() +
                ", \n    User ID = " + user.getId() +
                '}';
    }

}