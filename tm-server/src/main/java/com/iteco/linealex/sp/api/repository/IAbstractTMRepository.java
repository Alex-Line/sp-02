package com.iteco.linealex.sp.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IAbstractTMRepository<T> extends IRepository<T> {

    @Nullable
    public T findOneByName(@NotNull final String entityName);

    @Nullable
    public T findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    );

    @NotNull
    public Collection<T> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    );

    @NotNull
    public Collection<T> findAllByName(@NotNull final String pattern);

    @NotNull
    public Collection<T> findAllByName(
            @NotNull final String pattern,
            @NotNull final String projectId,
            @NotNull final String userId
    );

    @NotNull
    public Collection<T> findAllSortedByStartDate(
            @NotNull final String projectId,
            @NotNull final String userId
    );

    @NotNull
    public Collection<T> findAllSortedByStartDate(@NotNull final String userId);

    @NotNull
    public Collection<T> findAllSortedByStartDate();

    @NotNull
    public Collection<T> findAllSortedByFinishDate(
            @NotNull final String projectId,
            @NotNull final String userId
    );

    @NotNull
    public Collection<T> findAllSortedByFinishDate(@NotNull final String userId);

    @NotNull
    public Collection<T> findAllSortedByFinishDate();

    @NotNull
    public Collection<T> findAllSortedByStatus(
            @NotNull final String projectId,
            @NotNull final String userId);

    @NotNull
    public Collection<T> findAllSortedByStatus(@NotNull final String userId);

    @NotNull
    public Collection<T> findAllSortedByStatus();

}