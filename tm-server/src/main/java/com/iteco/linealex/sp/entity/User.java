package com.iteco.linealex.sp.entity;

import com.iteco.linealex.sp.dto.UserDto;
import com.iteco.linealex.sp.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "users")
public final class User extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    @Basic(optional = false)
    private String login = "unnamed";

    @Nullable
    @Basic(optional = false)
    private String hashPassword;

    @NotNull
    @Basic(optional = false)
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.ORDINARY_USER;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<Session> sessions = new ArrayList<>();

    public User(
            @NotNull final String login,
            @NotNull final String hashPassword
    ) {
        this.login = login;
        this.hashPassword = hashPassword;
    }

    @Nullable
    public static UserDto toUserDto(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setHashPassword(user.getHashPassword());
        userDto.setRole(user.getRole());
        userDto.setLogin(user.getLogin());
        return userDto;
    }

    @NotNull
    public static List<UserDto> toUsersDto(@NotNull final List<User> users) {
        @NotNull final List<UserDto> result = new ArrayList<>();
        for (@Nullable final User user : users) {
            result.add(toUserDto(user));
        }
        return result;
    }

    @NotNull
    @Override
    public String toString() {
        return "User " + login +
                ",\n     role = " + role +
                ",\n     id = " + super.getId() +
                ",\n     hashPassword = " + hashPassword;
    }

}