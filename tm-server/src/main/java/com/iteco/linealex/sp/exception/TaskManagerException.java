package com.iteco.linealex.sp.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class TaskManagerException extends Exception {

    @NotNull
    @Override
    public String getMessage() {
        return "[UNDEFINED TASK MANAGER EXCEPTION. COMMAND WAS INTERRUPTED]\n";
    }

}
