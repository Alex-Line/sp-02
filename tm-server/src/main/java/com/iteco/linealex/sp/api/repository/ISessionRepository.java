package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.entity.Session;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISessionRepository extends CrudRepository<Session, String> {

    void deleteAllByUserId(String userId);

}