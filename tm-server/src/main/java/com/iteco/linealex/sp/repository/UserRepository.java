package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.transaction.Transactional;
import java.util.Collection;

public final class UserRepository extends AbstractRepository<User> {

    public boolean contains(@NotNull final String entityId) {
        @NotNull final User user = findOne(entityId);
        if (user != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Nullable
    public User findOneByName(
            @NotNull final String login,
            @NotNull final String hashPassword
    ) {
        return entityManager.createQuery("SELECT u FROM User u " +
                "WHERE u.login= :login and u.hashPassword= :hashPassword", User.class)
                .setParameter("login", login).setParameter("hashPassword", hashPassword)
                .getSingleResult();
    }

    @Override
    public void persist(@NotNull final User example) {
        entityManager.persist(example);
    }

    @Override
    public void persist(@NotNull final Collection<User> collection) {
        for (@Nullable final User user : collection) {
            if (user == null) continue;
            entityManager.persist(user);
        }
    }

    @NotNull
    @Override
    public Collection<User> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT u FROM User u " +
                "WHERE u.id= :userId", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String entityId) {
        return entityManager.find(User.class, entityId);

    }

    @Override
    public void merge(@NotNull final User example) {
        entityManager.merge(example);
    }

    @Override
    public void remove(@NotNull final String entityId) {
        entityManager.createQuery("DELETE FROM User u WHERE u.id= :entityId")
                .setParameter("entityId", entityId).executeUpdate();
    }

    @Override
    @Transactional
    public void removeAll() {
        entityManager.createQuery("DELETE FROM User u").executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM User u WHERE u.id= :userId")
                .setParameter("userId", userId).executeUpdate();
    }

}