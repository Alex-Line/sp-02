package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class SessionRepository extends AbstractRepository<Session> {

    @Override
    public boolean contains(@NotNull final String entityId) {
        @Nullable final Session session = findOne(entityId);
        if (session != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Session> findAll() {
        return entityManager.createQuery("SELECT s FROM Session s", Session.class).getResultList();
    }

    @Override
    public void remove(@NotNull final String entityId) {
        entityManager.createQuery("DELETE FROM Session s WHERE s.id= :entityId")
                .setParameter("entityId", entityId).executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Session s").executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Session s WHERE s.user.id= :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    @Override
    public Collection<Session> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT s FROM Session s WHERE s.user.id= :userId", Session.class)
                .setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String entityId) {
        return entityManager.find(Session.class, entityId);
    }

    @Override
    public void persist(@NotNull final Session example) {
        entityManager.persist(example);
    }

    @Override
    public void persist(@NotNull final Collection<Session> collection) {
        for (Session example : collection) {
            if (example == null) continue;
            persist(example);
        }
    }

    @Override
    public void merge(@NotNull final Session example) {
        entityManager.merge(example);
    }

}