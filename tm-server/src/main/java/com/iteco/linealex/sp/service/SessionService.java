package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.repository.ISessionRepository;
import com.iteco.linealex.sp.api.service.ISessionService;
import com.iteco.linealex.sp.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Transactional
@Service("sessionService")
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Autowired
    private ISessionRepository sessionRepository;

    @Nullable
    @Override
    public Session getEntityById(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        return sessionRepository.findById(entityId).get();
    }

    @NotNull
    @Override
    public Collection<Session> getAllEntities() {
        return (Collection<Session>) sessionRepository.findAll();
    }

    @Override
    @Transactional
    public void persist(@Nullable final Session entity) {
        if (entity == null) return;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return;
        sessionRepository.save(entity);
    }

    @Override
    @Transactional
    public void persist(@NotNull final Collection<Session> collection) {
        if (collection.isEmpty()) return;
        for (Session session : collection) {
            if (session == null) continue;
            persist(session);
        }

    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        sessionRepository.deleteById(entityId);
    }

    @Transactional
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        sessionRepository.deleteAll();
    }

    @Override
    @Transactional
    public void merge(@Nullable final Session entity) {
        if (entity == null) return;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return;
        sessionRepository.save(entity);
    }

}