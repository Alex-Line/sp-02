package com.iteco.linealex.sp.enumerate;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Status {

    PLANNED("planned", 1),
    PROCESSING("processing", 2),
    DONE("done", 3);

    @NotNull
    private String name;

    private int priority;

    Status(@NotNull final String name, final int priority) {
        this.name = name;
        this.priority = priority;
    }

}