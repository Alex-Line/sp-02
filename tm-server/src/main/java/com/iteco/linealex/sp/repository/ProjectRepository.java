package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public final class ProjectRepository extends AbstractTMRepository<Project> {

    @Override
    public boolean contains(@NotNull final String entityId) {
        @Nullable final Project project = findOne(entityId);
        if (project != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Project> findAll() {
        return entityManager.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        return entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public Collection<Project> findAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return entityManager.createQuery(
                "SELECT p FROM Project p WHERE p.id = :projectId AND p.user.id= :userId", Project.class)
                .setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "AND (p.name= :pattern OR p.description= :pattern)", Project.class).setParameter("userId", userId)
                .setParameter("pattern", pattern).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllByName(@NotNull final String pattern) {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "WHERE (p.name= :pattern OR p.description= :pattern)", Project.class)
                .setParameter("pattern", pattern).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "ORDER BY p.dateStart", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStartDate() {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "ORDER BY p.dateStart", Project.class).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "ORDER BY p.dateFinish", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByFinishDate() {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "ORDER BY p.dateFinish", Project.class).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.user.id= :userId " +
                "ORDER BY p.status", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Project> findAllSortedByStatus() {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "ORDER BY p.status", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String entityName) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE p.name= : entityName", Project.class)
                .setParameter("entityName", entityName).getSingleResult();
    }

    @Nullable
    @Override
    public Project findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    ) {
        return entityManager.createQuery("SELECT p FROM Project p " +
                "WHERE p.name= : entityName AND p.user.id= : userId", Project.class)
                .setParameter("entityName", entityName).setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void persist(@NotNull final Project example) {
        entityManager.persist(example);
    }

    @Override
    public void persist(@NotNull final Collection<Project> collection) {
        for (Project example : collection) {
            entityManager.persist(example);
        }
    }

    @Override
    public void merge(@NotNull final Project example) {
        entityManager.merge(example);
    }

    @Override
    public void remove(@NotNull final String entityId) {
        entityManager.createQuery("DELETE FROM Project p WHERE p.id= :entityId")
                .setParameter("entityId", entityId).executeUpdate();
    }

    @Override
    @Transactional
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Project p").executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Project p WHERE p.user.id= :userId")
                .setParameter("userId", userId).executeUpdate();
    }

}