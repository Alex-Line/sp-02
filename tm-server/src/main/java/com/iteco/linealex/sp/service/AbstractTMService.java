package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.entity.AbstractTMEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public abstract class AbstractTMService<T extends AbstractTMEntity> extends AbstractService<T> {

    @Nullable
    @Override
    public abstract T getEntityById(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> getAllEntities() throws Exception;

    @Override
    public abstract void persist(@Nullable T entity) throws Exception;

    @Override
    public abstract void removeEntity(
            @Nullable final String entityId
    ) throws Exception;

    public abstract void removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @Override
    public abstract void removeAllEntities() throws Exception;

    @Override
    public abstract void merge(
            @Nullable final T entity
    ) throws Exception;

    @Nullable
    public abstract T getEntityByName(@Nullable final String entityName) throws Exception;

    @Nullable
    public abstract T getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStartDate() throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByFinishDate() throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStatus() throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    public abstract Collection<T> getAllEntities(
            final @Nullable String userId
    ) throws Exception;

}