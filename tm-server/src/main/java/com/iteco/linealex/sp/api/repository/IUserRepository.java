package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {

    User findOneByLoginAndHashPassword(String login, String hashPassword);

}