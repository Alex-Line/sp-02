package com.iteco.linealex.sp.api.service;

import com.iteco.linealex.sp.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    void removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

    void removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @Nullable
    Task getEntityByName(
            @NotNull final String taskName
    ) throws Exception;

    @Nullable
    Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String taskName
    ) throws Exception;

    @Nullable
    Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntities(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesByName(
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByStartDate() throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String project
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByFinishDate() throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByStatus() throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId
    ) throws Exception;

    @NotNull
    Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception;

}