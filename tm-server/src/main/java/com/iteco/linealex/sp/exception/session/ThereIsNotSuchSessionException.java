package com.iteco.linealex.sp.exception.session;

import com.iteco.linealex.sp.exception.TaskManagerException;
import org.jetbrains.annotations.NotNull;

public class ThereIsNotSuchSessionException extends TaskManagerException {

    @Override
    public @NotNull String getMessage() {
        return "THERE IS NOT SUCH SESSION. YOU HAVE TO LOG IN AGAIN";
    }

}