package com.iteco.linealex.sp.endpoint;

import com.iteco.linealex.sp.api.service.ISessionService;
import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.entity.Session;
import com.iteco.linealex.sp.exception.session.SessionExpiredException;
import com.iteco.linealex.sp.exception.session.ThereIsNotSuchSessionException;
import com.iteco.linealex.sp.util.ApplicationUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AbstractEndpoint {

    @Autowired
    ISessionService sessionService;

    @Value("SESSION_LIFE_TIME")
    String sessionLifeTime;

    @WebMethod
    public void validateSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto userSession
    ) throws Exception {
        if (userSession == null) throw new ThereIsNotSuchSessionException();
        @Nullable final Session session = sessionService.getEntityById(userSession.getId());
        if (session == null) throw new ThereIsNotSuchSessionException();
        if (session.getSignature() == null) throw new ThereIsNotSuchSessionException();
        if (!session.getSignature().equals(ApplicationUtils.getSignature(userSession)))
            throw new ThereIsNotSuchSessionException();
        if (new Date().getTime() - session.getCreationDate().getTime() >
                Long.parseLong(sessionLifeTime)) throw new SessionExpiredException();
    }

}