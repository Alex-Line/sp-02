package com.iteco.linealex.sp.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.sp.enumerate.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractTMEntity extends AbstractEntity {

    @Basic
    @Nullable
    String name = "unnamed";

    @Basic
    @Nullable
    String description = "";

    @Nullable
    @Basic(optional = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateStart = new Date();

    @Nullable
    @Basic(optional = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateFinish = new Date();

    @NotNull
    @ManyToOne
    @Basic(optional = false)
    User user;

    @Basic
    @NotNull
    @Enumerated(value = EnumType.STRING)
    Status status = Status.PLANNED;

}