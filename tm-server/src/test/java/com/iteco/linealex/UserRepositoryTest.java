package com.iteco.linealex;

import com.iteco.linealex.sp.api.repository.IUserRepository;
import com.iteco.linealex.sp.config.ApplicationConfig;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
@ContextConfiguration(classes = ApplicationConfig.class)
public class UserRepositoryTest {

    @Autowired
    private IUserRepository userRepository;

    @Before
    @Rollback
    public void setUp() {

    }

//    @Test
//    public void persistUser(){
//        User user = new User();
//        user.setLogin("test");
//        user.setRole(Role.ADMINISTRATOR);
//        user.setHashPassword("11111111");
//        userRepository.save(user);
//        System.out.println(userRepository.findById(user.getId()));
//    }

    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }
}
