package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-show";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ANY USER BY LOGIN. (AVAILABLE FOR ADMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) {
            System.out.println("[YOU MUST BE AUTHORISED TO DO THAT]\n");
            return;
        }
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        if (login.equals(selectedUser.getName())) {
            System.out.println(selectedUser);
            System.out.println();
            return;
        }
        @Nullable final User user = serviceLocator.getUserService().getUser(login, selectedUser);
        System.out.println(user);
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}