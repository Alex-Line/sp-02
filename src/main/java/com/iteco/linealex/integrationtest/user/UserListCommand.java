package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.LowAccessLevelException;
import com.iteco.linealex.sp.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class UserListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL USERS REGISTERED IN THE TASK MANAGER. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        @NotNull final Collection<User> collection = serviceLocator.getUserService().getUsers(selectedUser);
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY USERS YET]\n");
            return;
        }
        int index = 1;
        for (User user : collection) {
            System.out.println(index + ". " + user);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}