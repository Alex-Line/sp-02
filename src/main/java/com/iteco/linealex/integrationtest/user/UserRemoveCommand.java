package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.LowAccessLevelException;
import com.iteco.linealex.sp.exception.UserIsNotExistException;
import com.iteco.linealex.sp.exception.UserIsNotLogInException;
import com.iteco.linealex.sp.exception.WrongPasswordException;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVING THE USER BY LOGIN";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        if (serviceLocator.getUserService().getUser(login, selectedUser) == null)
            throw new UserIsNotExistException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR || selectedUser.getName().equals(login))
            throw new LowAccessLevelException();
        System.out.println("ENTER USER PASSWORD");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password);
        if (hashPassword.equals(selectedUser.getHashPassword())) {
            serviceLocator.getUserService().removeEntity(login);
            serviceLocator.getProjectService().removeAllEntities(selectedUser.getId());
            serviceLocator.getTaskService().removeAllTasks(selectedUser.getId());
            System.out.println("[USER " + selectedUser.getName() + " REMOVED]");
            System.out.println("[ALSO WAS REMOVED ALL IT'S PROJECTS AND TASKS]");
            System.out.println("[OK]\n");
        } else throw new WrongPasswordException();
    }

    @Override
    public boolean secure() {
        return true;
    }

}