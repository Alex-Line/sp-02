package com.iteco.linealex.sp.constant;

import java.io.File;

public class Constants {

    public static final String SAVE_DIR = System.getProperty("user.dir") + File.separator
            + "target" + File.separator + "taskmanager" + File.separator + "data" + File.separator;

    public static final String PASSWORD_SALT = "tzEGMt5k";

    public static final Integer PASSWORD_TIMES = 5;

    public static final String SESSION_SALT = "3D5x1i9el";

}