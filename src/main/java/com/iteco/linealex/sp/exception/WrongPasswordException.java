package com.iteco.linealex.sp.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class WrongPasswordException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "YOU ENTER WRONG OR EMPTY PASSWORD. COMMAND WAS INTERRUPTED\n";
    }

}