package com.iteco.linealex.sp.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class ThereIsNotSuchFileException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "[THERE IS NOT SUCH FILE]";
    }

}