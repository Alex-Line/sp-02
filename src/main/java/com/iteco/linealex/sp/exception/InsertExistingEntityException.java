package com.iteco.linealex.sp.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class InsertExistingEntityException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "THERE IS SUCH ELEMENT ALREADY. COMMAND WAS INTERRUPTED\n";
    }

}