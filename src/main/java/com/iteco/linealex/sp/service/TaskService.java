package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.service.ITaskService;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.exception.InsertExistingEntityException;
import com.iteco.linealex.sp.repository.AbstractRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class TaskService extends AbstractService<Task> implements ITaskService<Task> {

    public TaskService(@NotNull final AbstractRepository<Task> repository) {
        super(repository);
    }

    @Nullable
    public Task selectEntity(
            @Nullable final String projectId,
            @Nullable final String taskName,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return selectEntity(taskName, userId);
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(taskName, projectId, userId);
    }

    @Nullable
    public Task selectEntity(
            @Nullable final String taskName,
            @Nullable final String userId
    ) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.findOne(taskName, userId);
    }

    @NotNull
    public Collection<Task> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(userId);
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String projectId,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(projectId, userId);
    }

    @Nullable
    @Override
    public Task persist(@Nullable final Task entity) throws InsertExistingEntityException {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return repository.persist(entity);
    }

    @Nullable
    @Override
    public Task merge(@Nullable final Task entity) throws InsertExistingEntityException {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return null;
        if (entity.getUserId() == null || entity.getUserId().isEmpty()) return null;
        return repository.merge(entity);
    }

    @Nullable
    public Task removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, userId);
    }

    @Nullable
    public Task removeEntity(
            @Nullable final String projectId,
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return removeEntity(entityName, userId);
        if (entityName == null || entityName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return repository.remove(entityName, projectId, userId);
    }

    @NotNull
    public Collection<Task> removeAllTasksFromProject(
            @Nullable final String projectId,
            @Nullable final String userId
    ) {
        if (projectId == null || projectId.isEmpty()) return removeAllTasks(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.removeAll(userId, projectId);
    }

    @NotNull
    public Collection<Task> removeAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.removeAll(userId);
    }

    public boolean attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity
    ) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (selectedEntity == null) return false;
        for (@NotNull final Task task : repository.findAll()) {
            if (task.getProjectId() != null) continue;
            if (!task.getId().equals(selectedEntity.getId())) continue;
            task.setProjectId(projectId);
            return true;
        }
        return false;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(@Nullable String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(pattern);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(@Nullable String pattern, @Nullable String userId) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(pattern, userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(@Nullable String pattern, @Nullable String projectId, @Nullable String userId) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(pattern, projectId, userId);
    }

    @NotNull
    @Override
    public Collection<Task> getEntitiesSortedByStartDate() {
        return repository.findAllSortedByStartDate();
    }

    @NotNull
    @Override
    public Collection<Task> getEntitiesSortedByStartDate(@Nullable String userId) {
        if (userId == null) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStartDate(userId);
    }

    @NotNull
    @Override
    public Collection<Task> getEntitiesSortedByStartDate(
            @Nullable String project,
            @Nullable String userId
    ) {
        if (project == null) return Collections.EMPTY_LIST;
        if (userId == null) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStartDate(project, userId);
    }

    @NotNull
    @Override
    public Collection<Task> getEntitiesSortedByFinishDate() {
        return repository.findAllSortedByFinishDate();
    }

    @NotNull
    @Override
    public Collection<Task> getEntitiesSortedByFinishDate(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByFinishDate(userId);
    }

    @NotNull
    @Override
    public Collection<Task> getEntitiesSortedByFinishDate(
            @Nullable String projectId,
            @Nullable String userId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByFinishDate(projectId, userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus() {
        return repository.findAllSortedByStatus();
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStatus(userId);
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable String projectId,
            @Nullable String userId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStatus(projectId, userId);
    }

}