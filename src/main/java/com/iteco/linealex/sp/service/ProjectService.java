package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.service.IProjectService;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.exception.InsertExistingEntityException;
import com.iteco.linealex.sp.repository.AbstractRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class ProjectService extends AbstractService<Project> implements IProjectService<Project> {

    public ProjectService(@NotNull final AbstractRepository<Project> repository) {
        super(repository);
    }

    @Nullable
    @Override
    public Project persist(@Nullable final Project project) throws InsertExistingEntityException {
        if (project == null) return null;
        if (project.getName() == null || project.getName().isEmpty()) return null;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return null;
        if (project.getUserId() == null || project.getUserId().isEmpty()) return null;
        return repository.persist(project);
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project entity) {
        if (entity == null) return null;
        if (entity.getName() == null) return null;
        if (entity.getUserId() == null) return null;
        @Nullable final Project existedProject = repository.findOne(entity.getName(), entity.getUserId());
        if (existedProject == null) return null;
        existedProject.setDescription(entity.getDescription());
        existedProject.setDateStart(entity.getDateStart());
        existedProject.setDateFinish(entity.getDateFinish());
        return existedProject;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAll(userId);
    }

    @NotNull
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String pattern,
            @Nullable final String userId
    ) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(pattern, userId);
    }

    @NotNull
    public Collection<Project> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllByName(pattern);
    }

    @NotNull
    @Override
    public Collection<Project> getEntitiesSortedByStartDate() {
        return repository.findAllSortedByStartDate();
    }

    @NotNull
    @Override
    public Collection<Project> getEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStartDate(userId);
    }

    @Override
    public @NotNull Collection<Project> getEntitiesSortedByFinishDate() {
        return repository.findAllSortedByFinishDate();
    }

    @Override
    public @NotNull Collection<Project> getEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByFinishDate(userId);
    }

    @Override
    public @NotNull Collection<Project> getAllEntitiesSortedByStatus() {
        return repository.findAllSortedByStatus();
    }

    @Override
    public @NotNull Collection<Project> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.findAllSortedByStatus(userId);
    }

    @Nullable
    @Override
    public Project removeEntity(
            @Nullable final String projectName,
            @Nullable final String userId
    ) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final Project project = repository.remove(projectName, userId);
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return repository.removeAll(userId);
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String projectName,
            @Nullable final String userId
    ) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null) return null;
        return repository.findOne(projectName, userId);
    }

    @Nullable
    @Override
    public Project selectEntity(
            @Nullable final String entityName,
            @Nullable final String userId
    ) {
        if (entityName == null || entityName.isEmpty()) return null;
        return getEntityByName(entityName, userId);
    }

}