package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.service.ITerminalService;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public final class TerminalService implements ITerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @NotNull
    @Override
    public String nextLine() {
        return scanner.nextLine().trim();
    }

}