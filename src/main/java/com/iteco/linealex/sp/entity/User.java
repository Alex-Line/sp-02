package com.iteco.linealex.sp.entity;

import com.iteco.linealex.sp.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String hashPassword;

    @Nullable
    private Role role = Role.ORDINARY_USER;

    public User(@NotNull final String login,
                @NotNull final String hashPassword) {
        super.setName(login);
        this.hashPassword = hashPassword;
    }

    @NotNull
    @Override
    public String toString() {
        return "User " + getName() +
                ",\n     role = " + role.getDisplayName() +
                ",\n     id = " + super.getId() +
                '}';
    }

}