package com.iteco.linealex.sp.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.sp.enumerate.Status;
import com.iteco.linealex.sp.util.DateFormatter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity implements Serializable {

    @Nullable
    private String name = "unnamed task";

    @Nullable
    private String description = "";

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    private Date dateStart = new Date();

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    private Date dateFinish = new Date();

    @Nullable
    private String projectId = null;

    @Nullable
    private String userId = null;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    @Override
    public String toString() {
        return "Task " + name + " {" +
                "ID = " + super.getId() +
                ", \n    description='" + description + '\'' +
                ", \n    Start date = " + DateFormatter.formatDateToString(dateStart) +
                ", \n    Finish date = " + DateFormatter.formatDateToString(dateFinish) +
                ", \n    Status = " + status.getName() +
                ", \n    PROJECT_ID = " + projectId +
                ", \n    User ID = " + userId +
                '}';
    }

}