package com.iteco.linealex.sp.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService<Task> extends IService<Task> {

    @NotNull
    Collection<Task> removeAllTasksFromProject(
            @Nullable final String projectName,
            @Nullable final String id);

    @Nullable
    public Task selectEntity(
            @Nullable final String projectId,
            @Nullable final String taskName,
            @Nullable final String userId);

    @Nullable
    public Task selectEntity(
            @Nullable final String taskName,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String projectId,
            @Nullable final String userId);

    @Nullable
    public Task removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId);

    @Nullable
    public Task removeEntity(
            @Nullable final String projectId,
            @Nullable final String entityName,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> removeAllTasks(
            @Nullable final String userId);

    public boolean attachTaskToProject(
            @Nullable final String projectId,
            @Nullable final Task selectedEntity);

    @NotNull
    public Collection<Task> getAllEntitiesByName(@Nullable final String pattern);

    @NotNull
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String pattern,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String pattern,
            @Nullable final String projectId,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getEntitiesSortedByStartDate();

    @NotNull
    public Collection<Task> getEntitiesSortedByStartDate(@Nullable final String userId);

    @NotNull
    public Collection<Task> getEntitiesSortedByStartDate(
            @Nullable final String project,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getEntitiesSortedByFinishDate();

    @NotNull
    public Collection<Task> getEntitiesSortedByFinishDate(@Nullable final String userId);

    @NotNull
    public Collection<Task> getEntitiesSortedByFinishDate(
            @Nullable final String projectId,
            @Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus();

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus(@Nullable final String userId);

    @NotNull
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String projectId,
            @Nullable final String userId);

}