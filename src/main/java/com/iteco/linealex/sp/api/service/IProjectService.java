package com.iteco.linealex.sp.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IProjectService<Project> extends IService<Project> {

    @NotNull
    public Collection<Project> getAllEntities(@Nullable final String userId);

    @Nullable
    public Project getEntityByName(
            @Nullable final String entityName,
            @Nullable final String userId);

    @Nullable
    public Project removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId);

    @NotNull
    public Collection<Project> removeAllEntities(@Nullable final String userId);

    @NotNull
    public Collection<Project> getAllEntitiesByName(@Nullable final String pattern);

    @NotNull
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String pattern,
            @Nullable final String userId);

    @NotNull
    public Collection<Project> getEntitiesSortedByStartDate();

    @NotNull
    public Collection<Project> getEntitiesSortedByStartDate(@Nullable final String userId);

    @NotNull
    public Collection<Project> getEntitiesSortedByFinishDate();

    @NotNull
    public Collection<Project> getEntitiesSortedByFinishDate(@Nullable final String userId);

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStatus();

    @NotNull
    public Collection<Project> getAllEntitiesSortedByStatus(@Nullable final String userId);

}