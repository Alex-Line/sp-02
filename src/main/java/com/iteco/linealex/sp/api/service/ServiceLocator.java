package com.iteco.linealex.sp.api.service;

import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IProjectService<Project> getProjectService();

    @NotNull
    ITaskService<Task> getTaskService();

    @NotNull
    IUserService<User> getUserService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    ISelectedEntityService getSelectedEntityService();

    @NotNull
    ICommandService getCommandService();

}