package com.iteco.linealex.sp;

import com.iteco.linealex.sp.context.Bootstrap;
import org.jetbrains.annotations.NotNull;

public final class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}