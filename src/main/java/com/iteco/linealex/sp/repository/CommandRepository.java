package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.api.repository.ICommandRepository;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private Map<String, AbstractCommand> commands = new HashMap<>();

    @Override
    public void addCommand(@NotNull final AbstractCommand command) {
        commands.put(command.command(), command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@NotNull final String command) {
        for (@NotNull final Map.Entry<String, AbstractCommand> entry : commands.entrySet()) {
            if (entry.getValue().command().equals(command)) return entry.getValue();
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return commands.values();
    }

}