package com.iteco.linealex.sp.util;

import com.iteco.linealex.sp.exception.WrongDateFormatException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateFormatter {

    @NotNull
    private final static SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static String formatDateToString(@Nullable final Date date) {
        if (date == null) return format.format(new Date());
        return format.format(date);
    }

    public static Date formatStringToDate(@Nullable final String stringDate) throws WrongDateFormatException {
        try {
            return format.parse(stringDate);
        } catch (ParseException e) {
            throw new WrongDateFormatException();
        }
    }

}