package com.iteco.linealex.sp.command.data.save;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.constant.Constants;
import com.iteco.linealex.sp.dto.Domain;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class DataSaveBinaryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-binary";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE ALL DATA INTO BINARY FILE THE CLASSIC JAVA WAY";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.load(serviceLocator);
        @NotNull final File saveDir = new File(Constants.SAVE_DIR);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(Constants.SAVE_DIR + "data.bin");
        @NotNull final FileOutputStream outputStream = new FileOutputStream(saveFile);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.flush();
        objectOutputStream.close();
        serviceLocator.getSelectedEntityService().clearSelection();
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}