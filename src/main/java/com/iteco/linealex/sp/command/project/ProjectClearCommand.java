package com.iteco.linealex.sp.command.project;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (user != null && user.getRole() == Role.ADMINISTRATOR) {
            @NotNull final Collection<Project> collection = serviceLocator.getProjectService().removeAllEntities(user.getId());
            if (collection.isEmpty()) return;
            System.out.println("[All PROJECTS REMOVED]\n");
            return;
        }
        System.out.println("[THERE IS NOT ANY " + user.getName() + "'s PROJECTS FOR REMOVING]\n");
        serviceLocator.getSelectedEntityService().setSelectedProject(null);
    }

    @Override
    public boolean secure() {
        return true;
    }

}