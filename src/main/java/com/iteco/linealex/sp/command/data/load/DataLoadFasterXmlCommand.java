package com.iteco.linealex.sp.command.data.load;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.constant.Constants;
import com.iteco.linealex.sp.dto.Domain;
import com.iteco.linealex.sp.exception.ThereIsNotSuchFileException;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Collections;

public class DataLoadFasterXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-faster-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM XML FILE BY FASTER_XML";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getSelectedEntityService().clearSelection();
        serviceLocator.getSelectedEntityService().setSelectedUser(null);
        @NotNull final File savedFile = new File(Constants.SAVE_DIR + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) serviceLocator.getUserService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) serviceLocator.getProjectService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) serviceLocator.getTaskService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getTaskService().persist(domain.getTasks());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}