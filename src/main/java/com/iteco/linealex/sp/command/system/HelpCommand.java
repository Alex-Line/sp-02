package com.iteco.linealex.sp.command.system;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL AVAILABLE COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        for (@NotNull final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.command() + " : " + command.description());
        }
    }

    @Override
    public boolean secure() {
        return false;
    }

}