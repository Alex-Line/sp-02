package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ONE TASK IN SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (user == null) throw new UserIsNotLogInException();
        System.out.println("[ENTER TASK NAME]");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        System.out.println("REMOVING TASK...");
        @Nullable Task task = null;
        if (serviceLocator.getSelectedEntityService().getSelectedProject() != null) {
            task = serviceLocator.getTaskService().removeEntity(
                    serviceLocator.getSelectedEntityService().getSelectedProject().getId(),
                    taskName, user.getId());
        } else task = serviceLocator.getTaskService().removeEntity(taskName, user.getId());
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}