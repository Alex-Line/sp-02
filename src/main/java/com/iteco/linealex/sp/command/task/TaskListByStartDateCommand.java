package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public class TaskListByStartDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list-start";
    }

    @NotNull
    @Override
    public String description() {
        return "LIST TASKS BY START DATE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        @Nullable final Project selectedProject = serviceLocator.getSelectedEntityService().getSelectedProject();
        System.out.println("[TASK LIST]");
        @NotNull Collection<Task> collection = Collections.EMPTY_LIST;
        if (selectedUser.getRole() == Role.ADMINISTRATOR)
            collection = serviceLocator.getTaskService().getEntitiesSortedByStartDate();
        else if (selectedProject != null)
            collection = serviceLocator.getTaskService().getEntitiesSortedByStartDate(
                    selectedProject.getId(), selectedUser.getId());
        else collection = serviceLocator.getTaskService().getEntitiesSortedByStartDate(
                    selectedUser.getId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (Task task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}