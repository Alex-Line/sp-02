package com.iteco.linealex.sp.command.system;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class ClearSelectionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "selection-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "RESET SELECTION TASK AND PROJECT";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getSelectedEntityService().setSelectedProject(null);
        serviceLocator.getSelectedEntityService().setSelectedTask(null);
        System.out.println("[ALL SELECTION WAS CANCELED]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}