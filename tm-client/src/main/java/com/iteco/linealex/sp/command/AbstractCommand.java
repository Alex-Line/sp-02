package com.iteco.linealex.sp.command;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected Bootstrap bootstrap;

    @NotNull
    protected List<Role> roles = Arrays.asList(Role.ADMINISTRATOR, Role.ORDINARY_USER);

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;

    public boolean secure() {
        return bootstrap.getSession() != null;
    }

    public void printUser(@NotNull final UserDto user) {
        System.out.println("User " + user.getLogin() +
                ",\n     role = " + user.getRole() +
                ",\n     id = " + user.getId() +
                ",\n     hashPassword = " + user.getHashPassword()
        );
    }

}