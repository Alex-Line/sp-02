package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.api.endpoint.ProjectDto;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component(value = "task-list")
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL TASKS IN THE SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        @Nullable final ProjectDto selectedProject = bootstrap.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        @NotNull Collection<TaskDto> collection = Collections.EMPTY_LIST;
        if (selectedProject != null) {
            collection = bootstrap.getTaskEndpoint()
                    .getAllTasksWithUserIdAndProjectId(session, session.getUserId(), selectedProject.getId());
        } else collection = bootstrap.getTaskEndpoint().getAllTasksWithUserId(session, session.getUserId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANYTHING TO LIST]\n");
            return;
        }
        System.out.println("[TASK LIST]");
        int index = 1;
        for (@NotNull final TaskDto task : collection) {
            System.out.println(index + ". " + "Task " + task.getName() + " {" +
                    "ID = " + task.getId() +
                    ", \n    description='" + task.getDescription() + '\'' +
                    ", \n    Start date = " + task.getDateStart() +
                    ", \n    Finish date = " + task.getDateFinish() +
                    ", \n    Status = " + task.getStatus().name() +
                    ", \n    PROJECT_ID = " + task.getProjectId() +
                    ", \n    User ID = " + task.getUserId() +
                    '}');
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}