package com.iteco.linealex.sp.command.system;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "about")
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "PRINT BUILD NUMBER";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String buildNumber = Manifests.read("buildNumber");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("build number: " + buildNumber + "\n" + "developer: " + developer);
    }

    @Override
    public boolean secure() {
        return false;
    }

}