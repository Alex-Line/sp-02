package com.iteco.linealex.sp.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iteco.linealex.sp.api.endpoint.Session;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class ApplicationUtils {

    /**
     * Utils for work with dates
     */
    @NotNull
    private final static SimpleDateFormat FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static String formatDateToString(@Nullable final Date date) {
        if (date == null) return FORMAT.format(new Date());
        return FORMAT.format(date);
    }

    public static Date formatStringToDate(@Nullable final String stringDate) throws TaskManagerException_Exception {
        try {
            return FORMAT.parse(stringDate);
        } catch (ParseException e) {
            throw new TaskManagerException_Exception();
        }
    }

    /**
     * Utils for converting date from Gregorian calendar to XML format
     */
    @Nullable
    public static XMLGregorianCalendar toXMLGregorianCalendar(@NotNull final Date date) throws Exception {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        return xmlCalendar;
    }

    /**
     * Utils for work with signatures
     */
    @NotNull
    public static String getSignature(
            @NotNull final Session userSession,
            @Nullable final String salt,
            @Nullable final String times
    ) {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        try {
            @NotNull final String json = mapper.writeValueAsString(userSession);
            @NotNull String result = TransformatorToHashMD5.getHash(json, salt, Integer.parseInt(times));
            return result;
        } catch (JsonProcessingException | NoSuchAlgorithmException e) {
            return null;
        }
    }

}