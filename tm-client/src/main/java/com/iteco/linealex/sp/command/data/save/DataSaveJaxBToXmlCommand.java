package com.iteco.linealex.sp.command.data.save;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-save-jaxb-xml")
public class DataSaveJaxBToXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-jaxb-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO XML FILE BY JAXB";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO XML FILE");
        bootstrap.getUserEndpoint().saveJaxbXml(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}