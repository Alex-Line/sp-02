package com.iteco.linealex.sp.command.user;

import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value = "user-remove")
public final class UserRemoveCommand extends AbstractCommand {

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    @Override
    public String command() {
        return "user-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVING THE USER BY LOGIN";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final UserDto selectedUser = bootstrap.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = bootstrap.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD");
        @NotNull final String password = bootstrap.getTerminalService().nextLine();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                passwordSalt, Integer.parseInt(passwordTimes));
        if (hashPassword.equals(selectedUser.getHashPassword())) {
            @Nullable final UserDto user = bootstrap.getUserEndpoint().getUserByLogin(session, login, selectedUser);
            if (user == null) throw new TaskManagerException_Exception();
            bootstrap.getUserEndpoint().removeUserById(session, user.getId());
            bootstrap.getProjectEndpoint().removeAllProjectsByUserId(session, user.getId());
            bootstrap.getTaskEndpoint().removeAllTasksWithUserId(session, user.getId());
            System.out.println("[USER " + selectedUser.getLogin() + " REMOVED]");
            System.out.println("[ALSO WAS REMOVED ALL IT'S PROJECTS AND TASKS]");
            System.out.println("[OK]\n");
        } else throw new TaskManagerException_Exception();
    }

    @Override
    public boolean secure() {
        return true;
    }

}