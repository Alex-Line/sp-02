package com.iteco.linealex.sp.command.project;

import com.iteco.linealex.sp.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "project-clear")
public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        if (session.getRole() != Role.ADMINISTRATOR) {
            projectEndpoint.removeAllProjectsByUserId(session, session.getUserId());
            System.out.println("[All PROJECTS REMOVED]\n");
            return;
        } else {
            projectEndpoint.removeAllProjects(session);
            System.out.println("[All PROJECTS REMOVED]\n");
        }

        System.out.println("[THERE IS NOT ANY PROJECTS FOR REMOVING]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}