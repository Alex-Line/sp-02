package com.iteco.linealex.sp.command.data.load;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-load-faster-xml")
public class DataLoadFasterXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-faster-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM XML FILE BY FASTER_XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOADING DATA FROM XML FILE");
        bootstrap.getUserEndpoint().loadFasterXml(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}