package com.iteco.linealex.sp.command.data.save;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-save-faster-json")
public class DataSaveFasterJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-faster-json";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO JSON FILE BY FASTER-XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO JSON FILE");
        bootstrap.getUserEndpoint().saveFasterJson(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}