package com.iteco.linealex.sp.command.data.load;

import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-load-binary")
public class DataLoadBinaryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-binary";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD ALL DATA FROM BINARY FILE THE CLASSIC JAVA WAY";
    }

    @Override
    public void execute() throws TaskManagerException_Exception, Exception {
        System.out.println("LOADING DATA FROM BINARY FILE");
        bootstrap.getUserEndpoint().loadBinary(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}