package com.iteco.linealex.sp.command.data.save;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-save-jaxb-json")
public class DataSaveJaxBToJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-jaxb-json";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO JSON FILE BY JAXB";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO JSON FILE");
        bootstrap.getUserEndpoint().saveJaxbJson(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}