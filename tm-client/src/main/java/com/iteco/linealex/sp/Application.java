package com.iteco.linealex.sp;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.config.ApplicationConfig;
import com.iteco.linealex.sp.context.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public final class Application {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class,
                context.getBeansOfType(AbstractCommand.class).values());
        bootstrap.setCommands(context.getBeansOfType(AbstractCommand.class));
        bootstrap.start();
    }

}