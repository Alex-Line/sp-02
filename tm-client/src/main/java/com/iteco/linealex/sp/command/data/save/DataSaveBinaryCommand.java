package com.iteco.linealex.sp.command.data.save;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-save-binary")
public class DataSaveBinaryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-binary";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE ALL DATA INTO BINARY FILE THE CLASSIC JAVA WAY";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO BINARY FILE");
        bootstrap.getUserEndpoint().saveBinary(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}