package com.iteco.linealex.sp.command.system;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component(value = "help")
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL AVAILABLE COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        for (final Map.Entry<String, AbstractCommand> entry : bootstrap.getCommands().entrySet()) {
            System.out.println(entry.getValue().command() + " : " + entry.getValue().description());
        }
    }

    @Override
    public boolean secure() {
        return false;
    }

}