package com.iteco.linealex.integrationtest.task;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.lang.Exception;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RemoveAllTasksWithUserIdIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void removeAllTasksWithUserIdPositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest2 = new ProjectDto();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final TaskDto taskTest = new TaskDto();
        taskTest.setUserId(admin.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);

        @NotNull final TaskDto taskTest2 = new TaskDto();
        taskTest2.setUserId(admin.getId());
        taskTest2.setProjectId(projectTest.getId());
        taskTest2.setId(UUID.randomUUID().toString());
        taskTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setName("taskTest2");
        taskTest2.setDescription("taskTest2");
        taskTest2.setStatus(Status.PLANNED);

        @NotNull final TaskDto taskTest3 = new TaskDto();
        taskTest3.setUserId(admin.getId());
        taskTest3.setProjectId(projectTest2.getId());
        taskTest3.setId(UUID.randomUUID().toString());
        taskTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setName("taskTest3");
        taskTest3.setDescription("taskTest3");
        taskTest3.setStatus(Status.PLANNED);

        @NotNull final TaskDto taskTest4 = new TaskDto();
        taskTest4.setUserId(admin.getId());
        taskTest4.setProjectId(projectTest2.getId());
        taskTest4.setId(UUID.randomUUID().toString());
        taskTest4.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setName("taskTest4");
        taskTest4.setDescription("taskTest4");
        taskTest4.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest2);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest3);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest4);

        bootstrap.getTaskEndpoint().removeAllTasksWithUserId(adminSession, admin.getId());

        @NotNull TaskDto returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId());
        assertNull(returnedTask);
        returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest2.getId());
        assertNull(returnedTask);
        returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest3.getId());
        assertNull(returnedTask);
        returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest4.getId());
        assertNull(returnedTask);
        /**
         * Clean up
         */
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(adminSession, admin.getId(), projectTest.getId());
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(adminSession, admin.getId(), projectTest2.getId());
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest3.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest4.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void removeAllTasksWithUserIdPositive2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest2 = new ProjectDto();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final TaskDto taskTest = new TaskDto();
        taskTest.setUserId(user.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);

        @NotNull final TaskDto taskTest2 = new TaskDto();
        taskTest2.setUserId(user.getId());
        taskTest2.setProjectId(projectTest.getId());
        taskTest2.setId(UUID.randomUUID().toString());
        taskTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest2.setName("taskTest2");
        taskTest2.setDescription("taskTest2");
        taskTest2.setStatus(Status.PLANNED);

        @NotNull final TaskDto taskTest3 = new TaskDto();
        taskTest3.setUserId(user.getId());
        taskTest3.setProjectId(projectTest2.getId());
        taskTest3.setId(UUID.randomUUID().toString());
        taskTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest3.setName("taskTest3");
        taskTest3.setDescription("taskTest3");
        taskTest3.setStatus(Status.PLANNED);

        @NotNull final TaskDto taskTest4 = new TaskDto();
        taskTest4.setUserId(user.getId());
        taskTest4.setProjectId(projectTest2.getId());
        taskTest4.setId(UUID.randomUUID().toString());
        taskTest4.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest4.setName("taskTest4");
        taskTest4.setDescription("taskTest4");
        taskTest4.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest2);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest3);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest4);

        bootstrap.getTaskEndpoint().removeAllTasksWithUserId(userSession, user.getId());

        @NotNull TaskDto returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest.getId());
        assertNull(returnedTask);
        returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest2.getId());
        assertNull(returnedTask);
        returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest3.getId());
        assertNull(returnedTask);
        returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest4.getId());
        assertNull(returnedTask);
        /**
         * Clean up
         */
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(userSession, user.getId(), projectTest.getId());
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(userSession, user.getId(), projectTest2.getId());
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest2.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest3.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest4.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void removeAllTasksWithUserIdNegative1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final TaskDto taskTest = new TaskDto();
        taskTest.setUserId(admin.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getTaskEndpoint().removeAllTasksWithUserId(null, admin.getId());
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getTaskEndpoint().removeAllTasks(adminSession);
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId()));
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}