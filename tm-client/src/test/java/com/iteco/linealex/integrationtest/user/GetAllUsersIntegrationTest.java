package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetAllUsersIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void GetAllUsersPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @NotNull final List<UserDto> collection =
                new ArrayList<>(bootstrap.getUserEndpoint().getAllUsers(adminSession));
        assertEquals(collection.size(), 3);
        Collections.sort(collection, Comparator.comparing(UserDto::getLogin));
        assertEquals(collection.get(0).getId(), admin.getId());
        assertEquals(collection.get(1).getId(), newUser.getId());
        bootstrap.getUserEndpoint().removeUserById(bootstrap.getSession(), newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllUsersNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getAllUsers(null);
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}