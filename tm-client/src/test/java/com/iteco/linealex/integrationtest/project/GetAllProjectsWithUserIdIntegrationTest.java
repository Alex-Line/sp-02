package com.iteco.linealex.integrationtest.project;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.lang.Exception;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetAllProjectsWithUserIdIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void GetAllProjectsWithUserIdPositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(1234)));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(1)));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest2 = new ProjectDto();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(1234)));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(2343345)));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest3 = new ProjectDto();
        projectTest3.setUserId(null);
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(22222222)));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(11111111)));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.PLANNED);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest3);
        @NotNull final List<ProjectDto> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjectsWithUserId(adminSession, admin.getId()));
        assertFalse(collection.isEmpty());
        collection.sort(Comparator.comparing(ProjectDto::getName));
        assertEquals(collection.get(0).getId(), projectTest.getId());
        assertEquals(collection.get(1).getId(), projectTest2.getId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsWithUserIdPositive2() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest2 = new ProjectDto();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest3 = new ProjectDto();
        projectTest3.setUserId(admin.getId());
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.DONE);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest3);
        @NotNull final List<ProjectDto> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjectsWithUserId(userSession, user.getId()));
        assertFalse(collection.isEmpty());
        assertEquals(2, collection.size());
        collection.sort(Comparator.comparing(ProjectDto::getName));
        assertEquals(collection.get(0).getId(), projectTest.getId());
        assertEquals(collection.get(1).getId(), projectTest2.getId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsWithUserIdNegative1() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest2 = new ProjectDto();
        projectTest2.setUserId(admin.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest3 = new ProjectDto();
        projectTest3.setUserId(user.getId());
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(22222222)));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(11111111)));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.PLANNED);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest2);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest3);
        @NotNull final List<ProjectDto> collection = new ArrayList<>(bootstrap
                .getProjectEndpoint().getAllProjectsWithUserId(adminSession, admin.getId()));
        assertFalse(collection.isEmpty());
        assertNotEquals(3, collection.size());
        collection.sort(Comparator.comparing(ProjectDto::getName));
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetAllProjectsWithUserIdNegative2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest2 = new ProjectDto();
        projectTest2.setUserId(user.getId());
        projectTest2.setId(UUID.randomUUID().toString());
        projectTest2.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest2.setName("projectTest2");
        projectTest2.setDescription("projectTest2");
        projectTest2.setStatus(Status.PLANNED);

        @NotNull final ProjectDto projectTest3 = new ProjectDto();
        projectTest3.setUserId(UUID.randomUUID().toString());
        projectTest3.setId(UUID.randomUUID().toString());
        projectTest3.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date(1)));
        projectTest3.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date(2)));
        projectTest3.setName("projectTest3");
        projectTest3.setDescription("projectTest3");
        projectTest3.setStatus(Status.DONE);
        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest2);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().persistProject(userSession, projectTest3);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}