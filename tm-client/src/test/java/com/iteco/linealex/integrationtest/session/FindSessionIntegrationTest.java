package com.iteco.linealex.integrationtest.session;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

import static org.junit.Assert.*;

public class FindSessionIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void createSessionPositive1() throws Exception {
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertNotNull(adminSession);
        @Nullable final SessionDto session = bootstrap.getSessionEndpoint().findSession(adminSession.getId());
        assertNotNull(session);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void createSessionPositive2() throws Exception {
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertNotNull(userSession);
        @Nullable final SessionDto session = bootstrap.getSessionEndpoint().findSession(userSession.getId());
        assertNotNull(session);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void createSessionNegative1() throws Exception {
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertNotNull(adminSession);
        @Nullable final SessionDto session = bootstrap.getSessionEndpoint().findSession(null);
        assertNull(session);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void createSessionNegative2() throws Exception {
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertNotNull(userSession);
        @Nullable final SessionDto session = bootstrap.getSessionEndpoint().findSession(UUID.randomUUID().toString());
        assertNull(session);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

}