package com.iteco.linealex.integrationtest.session;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CreateSessionIntegrationClass {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void createSessionPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void createSessionPositive2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void createSessionNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            adminSession = bootstrap.getSessionEndpoint().createSession("admn", adminPassword);
        });
        assertNotNull(thrown.getMessage());
    }

    @Test
    void createSessionNegative2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            userSession = bootstrap.getSessionEndpoint().createSession(null, userPassword);
        });
        assertNotNull(thrown.getMessage());
    }

}