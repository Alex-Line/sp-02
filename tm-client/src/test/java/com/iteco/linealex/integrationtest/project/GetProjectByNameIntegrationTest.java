package com.iteco.linealex.integrationtest.project;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.lang.Exception;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetProjectByNameIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void GetProjectByNamePositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @Nullable final ProjectDto returnedProject = bootstrap.getProjectEndpoint()
                .getProjectByName(adminSession, projectTest.getName());
        assertNotNull(returnedProject);
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetProjectByNamePositive2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        @Nullable final ProjectDto returnedProject = bootstrap.getProjectEndpoint()
                .getProjectByName(adminSession, projectTest.getName());
        assertNotNull(returnedProject);
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetProjectByNameNegative1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);

        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().getProjectByName(adminSession, null);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void GetProjectByNameNegative2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        /**
         * Creating new projects
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);

        /**
         * Inserting projects and check returned from DB projects
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().getProjectByName(userSession, projectTest.getName());
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}